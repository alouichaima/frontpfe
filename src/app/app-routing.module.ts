import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminlayoutComponent } from './layouts/adminlayout/adminlayout.component';
import { ProfillayoutComponent } from './layouts/profillayout/profillayout/profillayout.component';
import { UserlayoutComponent } from './layouts/userlayout/userlayout.component';

const routes: Routes = [
  {path:'',component:UserlayoutComponent,children:[
    {path:'',loadChildren:()=>import('./views/user/home/home.module').then(m=>m.HomeModule)},
    {path:'login',loadChildren:()=>import('./views/user/signin/signin.module').then(m=>m.SigninModule)},
    {path:'register',loadChildren:()=>import('./views/user/signup/signup.module').then(m=>m.SignupModule)},
    {path:'espace',loadChildren:()=>import('./views/user/espace/espace.module').then(m=>m.EspaceModule)},
    {path:'activite', loadChildren:()=>import('./views/user/ACT/activite/activite.module').then(m=>m.ActiviteModule)},
    {path:'evenement', loadChildren:()=>import('./views/user/evenement/evenement.module').then(m=>m.EvenementModule)},


    {path:'contact', loadChildren:()=>import('./views/user/ACT/contact/contact.module').then(m=>m.ContactModule)},
    {path:'bodyattack', loadChildren:()=>import('./views/user/ACT/bodyattack/bodyattack.module').then(m=>m.BodyattackModule)},
    {path:'bodycombat', loadChildren:()=>import('./views/user/ACT/bodycombat/bodycombat.module').then(m=>m.BodycombatModule)},
    {path:'rpm', loadChildren:()=>import('./views/user/ACT/rpm/rpm.module').then(m=>m.RpmModule)},
    {path:'fitness', loadChildren:()=>import('./views/user/ACT/fitness/fitness.module').then(m=>m.FitnessModule)},
    {path:'cours', loadChildren:()=>import('./views/user/ACT/cours/cours.module').then(m=>m.CoursModule)},
    {path:'taekwondo', loadChildren:()=>import('./views/user/ACT/taekwondo/taekwondo.module').then(m=>m.TaekwondoModule)},
    {path:'zumba', loadChildren:()=>import('./views/user/ACT/zumba/zumba.module').then(m=>m.ZumbaModule)},

  ]},
  {path:'admin',component:AdminlayoutComponent,children:[
    {path:'',loadChildren:()=>import('./views/admin/dashboard/dashboard.module').then(m=>m.DashboardModule)},
    {path:'dashboard',loadChildren:()=>import('./views/admin/dashboard/dashboard.module').then(m=>m.DashboardModule)},
    {path:'allusers',loadChildren:()=>import('./views/admin/all-utilisateurs/all-utilisateurs.module').then(m=>m.AllUtilisateursModule)},
    {path:'addusers',loadChildren:()=>import('./views/admin/addutilisateur/addutilisateur.module').then(m=>m.AddutilisateurModule)},
    {path:'detailusers',loadChildren:()=>import('./views/admin/detail-utilisateur/detail-utilisateur.module').then(m=>m.DetailUtilisateurModule)},
    {path:'adminlogin',loadChildren:()=>import('./views/admin/adminlogin/adminlogin.module').then(m=>m.AdminloginModule)},
    {path:'editact',loadChildren:()=>import('./views/admin/activite/editact/editact/editact.module').then(m=>m.EditactModule)},
    {path:'listeact',loadChildren:()=>import('./views/admin/activite/listeact/listeact/listeact.module').then(m=>m.ListeactModule)},
    {path:'addact',loadChildren:()=>import('./views/admin/activite/addactivite/addact/addact.module').then(m=>m.AddactModule)},
    {path:'addevents',loadChildren:()=>import('./views/admin/evenement/addevents/addevents/addevents.module').then(m=>m.AddeventsModule)},
    {path:'list',loadChildren:()=>import('./views/admin/evenement/listevents/list/list.module').then(m=>m.ListModule)},
    {path:'editevents',loadChildren:()=>import('./views/admin/evenement/editevents/editevents/editevents.module').then(m=>m.EditeventsModule)},
    {path:'editevents',loadChildren:()=>import('./views/admin/evenement/editevents/editevents/editevents.module').then(m=>m.EditeventsModule)},
    {path:'addespace',loadChildren:()=>import('./views/admin/espace/addespace/addespace.module').then(m=>m.AddespaceModule)},
    {path:'editespace',loadChildren:()=>import('./views/admin/espace/editespace/editespace.module').then(m=>m.EditespaceModule)},
    {path:'listespace',loadChildren:()=>import('./views/admin/espace/listespace/listespace.module').then(m=>m.ListespaceModule)},







  ]},

  {path:'profil',component:ProfillayoutComponent,children:[
    {path:'events',loadChildren:()=>import('./views/profiluser/events/events/events.module').then(m=>m.EventsModule)},
    {path:'monprofil',loadChildren:()=>import('./views/profiluser/monprofil/monprofil/monprofil.module').then(m=>m.MonprofilModule)},
    {path:'list',loadChildren:()=>import('./views/profiluser/listabonne/listabonne.module').then(m=>m.ListabonneModule)},




  ]},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
