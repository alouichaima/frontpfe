import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditactRoutingModule } from './editact-routing.module';
import { EditactComponent } from './editact.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    EditactComponent
  ],
  imports: [
    CommonModule,
    EditactRoutingModule,
    FormsModule
  ]
})
export class EditactModule { }
