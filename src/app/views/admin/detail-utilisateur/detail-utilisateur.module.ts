import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailUtilisateurRoutingModule } from './detail-utilisateur-routing.module';
import { DetailUtilisateurComponent } from './detail-utilisateur.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DetailUtilisateurComponent
  ],
  imports: [
    CommonModule,
    DetailUtilisateurRoutingModule,
    FormsModule
  ]
})
export class DetailUtilisateurModule { }
