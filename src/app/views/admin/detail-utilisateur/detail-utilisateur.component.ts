import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { utilisateur } from 'src/app/models/utilisateur.model';
import { UserCRUDService } from 'src/app/service/user-crud.service';

@Component({
  selector: 'app-detail-utilisateur',
  templateUrl: './detail-utilisateur.component.html',
  styleUrls: ['./detail-utilisateur.component.css']
})
export class DetailUtilisateurComponent implements OnInit {
  curuser : utilisateur = {
    nom:'',
    prenom:'',
    datenaiss:'',
    email:'',
    telephone:0,
    poids:0
  };

  message=""

  constructor(private UserCRUDService : UserCRUDService, 
              private route : ActivatedRoute,
              private router : Router
              ) { }

  ngOnInit(): void {
    this.message = '';
    this.getuser(this.route.snapshot.params.id);
  }

  getuser(id: string): void {
    this.UserCRUDService.get(id)
      .subscribe(
        data => {
          this.curuser = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updateuser(curuser:any): void {
    this.message = '';

    this.UserCRUDService.update(curuser)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message ? response.message : 'This User was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteuser(): void {
    this.UserCRUDService.delete(this.curuser.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/allusers']);
        },
        error => {
          console.log(error);
        });
  }


}
