import { Component, OnInit } from '@angular/core';
import { utilisateur } from 'src/app/models/utilisateur.model';
import { UserCRUDService } from 'src/app/service/user-crud.service';

@Component({
  selector: 'app-addutilisateur',
  templateUrl: './addutilisateur.component.html',
  styleUrls: ['./addutilisateur.component.css']
})
export class AddutilisateurComponent implements OnInit {
utilisateur : utilisateur ={
  nom:'',
  prenom:'',
  datenaiss:'',
  email:'',
  password:'',
  telephone:0,
  poids:0
};
submitted = false;
response : any

  constructor(private UserCRUDService: UserCRUDService) {}



  ngOnInit(): void {
  }

  saveutilisateur(): void {
    const data = {
      nom: this.utilisateur.nom,
      prenom: this.utilisateur.prenom,
      datenaiss: this.utilisateur.datenaiss,
      email: this.utilisateur.email,
      password:this.utilisateur.password,
      telephone: this.utilisateur.telephone,
      poids: this.utilisateur.poids

    };



    this.UserCRUDService.create(data)
      .subscribe(
        (        response: any) => {
          console.log(response);
          this.submitted = true;
        },
        (        error: any) => {
          console.log(error);
        });
  }

  newutilisateur(): void {
    this.submitted = false;
    this.utilisateur = {
      nom: '',
      prenom: '',
      datenaiss:'',
      email:'',
      password:'',
      telephone:0,
      poids:0
    };
  }

}
