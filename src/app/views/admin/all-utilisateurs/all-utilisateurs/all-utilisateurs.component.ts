import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { utilisateur } from 'src/app/models/utilisateur.model';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { UserCRUDService } from 'src/app/service/user-crud.service';

@Component({
  selector: 'app-all-utilisateurs',
  templateUrl: './all-utilisateurs.component.html',
  styleUrls: ['./all-utilisateurs.component.css']
})
export class AllUtilisateursComponent implements OnInit {

  roles: string[] = [];
  utilisateur?: utilisateur[];
  curuser: utilisateur = {};
  curIndex = -1;
  message=""
  

  constructor(private UserCRUDService: UserCRUDService, private tokenStorage: TokenStorageService , private route:Router) { }

  ngOnInit(): void {
    this.retrieveUS();
    this.roles = this.tokenStorage.getUser().roles;

  }

  retrieveUS(): void {
    this.UserCRUDService.getAll()
      .subscribe(
        data => {
          this.utilisateur = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveUS();
    this.curuser = {};
    this.curIndex = -1;
  }

  setActiveUS(utilisateur: utilisateur, index: number): void {
    this.curuser = utilisateur;
    this.curIndex = index;
  }
  deleteUser(): void {
    this.UserCRUDService.delete(this.curuser.id)
      .subscribe(
        response => {
          console.log(response);
         
        },
        error => {
          console.log(error);
        });
  }

  updateuser(curuser:any): void {
    this.message = '';
    this.route.navigate(['/detailusers'])
    this.UserCRUDService.update( curuser)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message ? response.message : 'This User was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }
  
  
}
