import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/service/token-storage.service';

@Component({
  selector: 'app-monprofil',
  templateUrl: './monprofil.component.html',
  styleUrls: ['./monprofil.component.css']
})
export class MonprofilComponent implements OnInit {

  GymUser: any;

  constructor(private token: TokenStorageService) { }

  ngOnInit(): void {
    this.GymUser = this.token.getUser();
  }

}
