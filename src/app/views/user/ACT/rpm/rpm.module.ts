import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RpmRoutingModule } from './rpm-routing.module';
import { RpmComponent } from './rpm.component';


@NgModule({
  declarations: [
    RpmComponent
  ],
  imports: [
    CommonModule,
    RpmRoutingModule
  ]
})
export class RpmModule { }
